# 勘误与建议(Corrigendum and Suggestions)



## 勘误列表(2022年12月第1版/2022年12月河北第1次印刷)

2023.02.01: 

1. 第88页倒数第2行：“applications.**jason**”应为“applications.**json**”。

2. 第196页表6-16的如下图所示两处位置的连接符”-“，需要删除：

   <img src="figures/Cor-and-Sug/Page196Table6-16.png" alt="Page196Table6-16" style="zoom:80%;" />

   【注意：书中部分表格还存在类似的排版问题(自动添加了英文单词的换行连接符)，已发现部分会尽量编入勘误列表中，未入列表中的，请读者积极反馈，谢谢。】

3. 第210/211/212/213页的表7-1及其续表中的如下图所示的5处位置的连接符”-“，需要删除：

   <img src="figures/Cor-and-Sug/Page210Table7-1.png" alt="Page210Table7-1" style="zoom: 40%;" />

   

   4.第236页的代码清单7-30中的两处双引号，需要改为英文状态下的双引号：

   <img src="figures/Cor-and-Sug/Page236CodeList7-30.png" alt="Page236CodeList7-30" style="zoom:60%;" />

   5.第244页的日志清单7-7的下一行文字，多了1个连接符“-”，需要去掉，如下图所示：

   <img src="figures/Cor-and-Sug/Page244Line1_UnderLogList7-7.png" alt="Page244Line1_UnderLogList7-7" style="zoom:80%;" />

   6.第260页表7-5的续表如下图所示两处位置的连接符”-“，需要删除：

   <img src="figures/Cor-and-Sug/Page260Table7-5.png" alt="Page260Table7-5" style="zoom:70%;" />

   7.第399页的图9-8左下角部分的“gpio_config”节点内的“child”字段自动换行了，不影响阅读理解，但不美观，建议修正。

   <img src="figures/Cor-and-Sug/Page399Pic9-8.png" alt="Page399Pic9-8" style="zoom:80%;" />

## 建议列表

1.Xxx

2.Xxx