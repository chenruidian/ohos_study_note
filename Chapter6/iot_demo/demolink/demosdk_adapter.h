/* Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: liangkz @ https://ost.51cto.com/column/46
 * Date  : 2022.04.01
 *
 */
#ifndef DEMOSDK_ADAPTER_H
#define DEMOSDK_ADAPTER_H

#define DEMOSDK_ERR (-1)
#define DEMOSDK_OK 0

struct TaskPara {
    char *name;
    void *(*func)(char* arg);
    void *arg;
    unsigned char prio;
    unsigned int size;
};

int DemoSdkCreateTask(unsigned int *handle, const struct TaskPara *para);
void DemoSdkSleepMs(int ms);

//default to init I2C0, and write to device by I2C0
unsigned int I2C_Init(void);
unsigned int I2C_Write(unsigned short deviceAddr, const unsigned char *data, unsigned int len);
#endif
