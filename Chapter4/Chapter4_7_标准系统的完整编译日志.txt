+++++++++++++++++++++++++++++++++++++++
--product-name Hi3516DV300 --ccache
---------------------------------------
build/build_scripts/build_standard.sh:
--product-name Hi3516DV300 --device-name hi3516dv300 --target-os ohos --target-cpu arm --ccache
CCACHE_DIR=/home/ohos/Ohos/LTS60G/B_LTS3/.ccache
Set cache size limit to 50.0 GB
---------------------------------------
build/build_scripts/build_common.sh: do_make_ohos...
build_ohos_cmd: build/build_scripts/build_ohos.sh product_name=Hi3516DV300 target_os=ohos target_cpu=arm gn_args=is_standard_system=true build_target=images
++++++++++++++++++++++++++++++++++++++++

build/build_scripts/build_ohos.sh:
product_name=Hi3516DV300 target_os=ohos target_cpu=arm gn_args=is_standard_system=true build_target=images
[3-1] build/core/build_scripts/pre_process.sh: Begin:
Python 3.8.5
--------------------------------------
OPTIONS=product_name
  PARAM=Hi3516DV300
OPTIONS=target_os
  PARAM=ohos
OPTIONS=target_cpu
  PARAM=arm
OPTIONS=gn_args
  PARAM=is_standard_system=true
OPTIONS=build_target
  PARAM=images
--------------------------------------
[3-1] build/core/build_scripts/pre_process.sh: End.

//build/core/build_scripts/make_main.sh: do_make
do_make: TARGET_OUT_DIR=/home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release
do_make: prepare to save build log into 'build.log'
product_name=Hi3516DV300 target_os=ohos target_cpu=arm gn_args=is_standard_system=true build_target=images

do_make: get_gn_parameters:
do_make: gn begin:
=======================================================
//build/core/gn/BUILD.gn: Entry:: Begin:
root_out_dir     = //out/ohos-arm-release
root_build_dir   = //out/ohos-arm-release
root_gen_dir     = //out/ohos-arm-release/gen
current_toolchain= //build/toolchain/ohos:ohos_clang_arm
host_toolchain   = //build/toolchain/linux:clang_x64

load_result:
//build/loader/load.py:
load.py: load: begin:#############################################
args: Namespace(build_platform_name='phone', build_xts=False, example_subsystem_file=None, gn_root_out_dir='/home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release', ignore_api_check=['xts', 'common', 'subsystem_examples'], platforms_config_file='/home/ohos/Ohos/LTS60G/B_LTS3/out/build_configs/standard_system/platforms.build', scalable_build=False, source_root_dir='/home/ohos/Ohos/LTS60G/B_LTS3/', subsystem_config_file='/home/ohos/Ohos/LTS60G/B_LTS3/out/build_configs/subsystem_config.json', target_cpu='arm', target_os='ohos')
load.py: load: create: config_output_relpath: out/ohos-arm-release/build_configs
load.py: load: create: //out/ohos-arm-release/build_configs/subsystem_info/*.json
load.py: load: create: //out/ohos-arm-release/build_configs/platforms_info/*.json
load.py: load: _all_platforms: {'phone': '//build/toolchain/ohos:ohos_clang_arm'}
load.py: load: build_platforms: ['phone']
load.py: load: loading ohos.build and gen part variant info into: //out/ohos-arm-release/build_configs/[31 folds with BUILD.gn]
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs/target_platforms_parts.json
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs/phone_system_capabilities.json
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs <-/phone-stub/*
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs/platforms_parts_by_src.json

load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs <-/parts_list.gni
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs <-/inner_kits_list.gni
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs <-/system_kits_list.gni
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs <-/parts_test_list.gni
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs <-/BUILD.gn

load.py: load: required_phony_targets: {}
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs/required_parts_targets.json
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs/required_parts_targets_list.json
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs/parts_src_flag.json
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs/platforms_list.gni
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs/parts_different_info.json
load.py: load: create: /home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release/build_configs/infos_for_testfwk.json & target_platforms_parts.json
load.py: load: end. #############################################

  Entry:: #build_configs# generation is complete.
  Entry:: begin to build product_name[Hi3516DV300]
  Entry:: [Hi3516DV300]: building group(make_all) +deps:[images/make_inner_kits/packages]
  Entry:: going to build //out/ohos-arm-release/build_configs/ [BUILD.gn and subdirs' *.gn]
//build/core/gn/BUILD.gn: Entry:: End.
=======================================================

//build/ohos/images/BUILD.gn: make_images
---------------------------------------------
//build/ohos/packages/BUILD.gn: make_packages
  :: make_packages: import //out/ohos-arm-release/build_configs/platforms_list.gni
  :: make_packages: deps=[":phone_install_modules", ":phone_parts_list", ":gen_required_modules_phone"]
  :: make_packages: current_platform_dir[//out/ohos-arm-release/packages/phone]
  :: make_images: deps=[":phone_system_image", ":phone_updater_image", ":phone_userdata_image", ":phone_vendor_image"]
  :: make_packages end.
---------------------------------------------

led_ctrl:: compile 'led_green' for ohos_standard(linux)
build_l2 = false
OHOS_PROFILER_DIR =  //developtools/profiler/
OHOS_PROFILER_3RDPARTY_DIR =  //third_party/
OHOS_PROFILER_3RDPARTY_GRPC_DIR =  //third_party//grpc
OHOS_PROFILER_3RDPARTY_PROTOBUF_DIR =  //third_party//protobuf
OHOS_PROFILER_3RDPARTY_GOOGLETEST_DIR //third_party//googletest
build_tests =  false
enable_debuginfo = false
enable_coverage = false
//device/hisilicon/build/BUILD.gn: building 'products_group' for both lite and standard system
led_ctrl:: compile 'led_red' for ohos_standard(linux)
product_name : , Hi3516DV300

//device/hisilicon/hi3516dv300/BUILD.gn: building 'hi3516dv300_group'
//device/hisilicon/hardware/BUILD.gn: building std 'hardware_group'
//device/hisilicon/modules/middleware/BUILD.gn: building std 'middleware_group'
arm
buffer manager use target: 
platform: ohos
platform: ohos
platform: windows
platform: mac
platform: ohos
platform: ohos
platform: ohos
platform: ohos
proto_out_dir //out/ohos-arm-release/gen/cpp/developtools/profiler/protos/services gen/cpp/developtools/profiler/protos/services
proto_out_dir //out/ohos-arm-release/gen/cpp/developtools/profiler/protos/types/plugins/bytrace_plugin gen/cpp/developtools/profiler/protos/types/plugins/bytrace_plugin
build_l2 = false
OHOS_PROFILER_DIR =  //developtools/profiler/
OHOS_PROFILER_3RDPARTY_DIR =  //third_party/
OHOS_PROFILER_3RDPARTY_GRPC_DIR =  //third_party//grpc
OHOS_PROFILER_3RDPARTY_PROTOBUF_DIR =  //third_party//protobuf
OHOS_PROFILER_3RDPARTY_GOOGLETEST_DIR //third_party//googletest
build_tests =  false
enable_debuginfo = false
enable_coverage = false
----------------------------------protobuf_cc
----------------------------------protobuf_cc
ABSEIL_DIR =  //third_party/abseil-cpp/
Done. Made 8680 targets from 1585 files in 161117ms
do_make: gn end.


do_make: BUILD_TARGET_NAME=  images


do_make: Starting Ninja...
python version: Python 3.8.5
do_make: build_target: images
ninja: Entering directory `/home/ohos/Ohos/LTS60G/B_LTS3/out/ohos-arm-release'
[1/1030] CXX obj/base/update/updater/services/script/yacc/libupdaterscript/lexer.o
......
[991/991] STAMP obj/build/core/gn/images.stamp


[3-3] build/core/build_scripts/post_process.sh: Begin:
used: 296 seconds
pycache statistics
manage pycache contents
pycache daemon exit
ccache statistics
---------------------------------------------
ccache summary:
cache hit (direct)  : 23
cache hit (preprocessed)  : 0
cache miss  : 2
hit rate:  92.00% 
mis rate: 8.00% 
---------------------------------------------
c targets overlap rate statistics
subsystem       	files NO.	percentage	builds NO.	percentage	overlap rate
ark             	     421	3.1%	     755	5.2%	1.79
utils           	     205	1.5%	     245	1.7%	1.20
third_party     	    6960	51.3%	    7553	51.9%	1.09
aafwk           	     203	1.5%	     203	1.4%	1.00
account         	      14	0.1%	      14	0.1%	1.00
ace             	    1257	9.3%	    1257	8.6%	1.00
appexecfwk      	     370	2.7%	     370	2.5%	1.00
applications    	       1	0.0%	       1	0.0%	1.00
ccruntime       	      29	0.2%	      29	0.2%	1.00
communication   	     464	3.4%	     464	3.2%	1.00
developtools    	      77	0.6%	      77	0.5%	1.00
distributeddatamgr	     331	2.4%	     331	2.3%	1.00
distributedhardware	      33	0.2%	      33	0.2%	1.00
distributedschedule	      40	0.3%	      40	0.3%	1.00
global          	      19	0.1%	      19	0.1%	1.00
graphic         	     133	1.0%	     133	0.9%	1.00
hdf             	      44	0.3%	      44	0.3%	1.00
hiviewdfx       	     135	1.0%	     135	0.9%	1.00
miscservices    	      62	0.5%	      62	0.4%	1.00
multimedia      	     272	2.0%	     272	1.9%	1.00
multimodalinput 	      29	0.2%	      29	0.2%	1.00
notification    	     127	0.9%	     127	0.9%	1.00
powermgr        	      50	0.4%	      50	0.3%	1.00
security        	     265	2.0%	     265	1.8%	1.00
startup         	      57	0.4%	      57	0.4%	1.00
telephony       	     225	1.7%	     225	1.5%	1.00
updater         	      81	0.6%	      81	0.6%	1.00
wpa_supplicant-2.9	     149	1.1%	     149	1.0%	1.00

c overall build overlap rate: 1.07


[3-3] build/core/build_scripts/post_process.sh: End.
=====build Hi3516DV300 successful.
++++++++++++++++++++++++++++++++++++++++

